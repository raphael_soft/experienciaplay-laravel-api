-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-11-2019 a las 13:45:55
-- Versión del servidor: 10.1.35-MariaDB
-- Versión de PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `webproject`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(30) COLLATE latin1_spanish_ci DEFAULT NULL,
  `store` varchar(30) COLLATE latin1_spanish_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `observations` text COLLATE latin1_spanish_ci,
  `quantity` float NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `name`, `store`, `status`, `observations`, `quantity`, `created_at`, `updated_at`) VALUES
(1, 'product 1', 'sas', 0, 'sdsd', 2, '2019-11-09 01:22:49', '2019-11-09 05:23:03'),
(2, 'dfs', 'Centro', 1, 'dsff', 2, '2019-11-09 06:11:08', '2019-11-09 16:44:08'),
(3, 'sdfsd', 'Centro', 1, '1sdd', 2, '2019-11-09 06:14:59', '2019-11-09 16:32:46'),
(4, 'ñlkñl', 'Occidente', 0, 'dasfsd', 55, '2019-11-09 06:20:45', '2019-11-09 06:20:45'),
(5, 'kijlkj', 'Centro', 1, 'dsfs', 2, '2019-11-09 06:23:44', '2019-11-09 16:44:13'),
(6, 'kjlk', 'Centro', 1, 'dsdfs', 21, '2019-11-09 06:24:22', '2019-11-09 06:24:22'),
(7, 'rgfg', 'Centro', 1, 'sdf', 22, '2019-11-09 06:25:31', '2019-11-09 06:25:31');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
