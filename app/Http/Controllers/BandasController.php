<?php

namespace PoultryWeb\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use PoultryWeb\Banda;
use Illuminate\Support\Facades\DB;

class BandasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // header("Access-Control-Allow-Origin: *");
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getBandas()
    {
        $bandas = Banda::groupBy('nombre')->orderBy('id','asc')->get();
        return response()->json($bandas);
    }

    public function getBanda($id)
    {
        $banda = Banda::find($id);
        return response()->json($banda);
    }

    public function sendReporte(Request $request){
        $resp = array( "resp" => "OK", "message" => "");
        $usuario = $request->input('user');
        $data = $request->input('data');
        //$colores = $request->input('colors');
        $colores = ['#e8d500', '#009cff', '#ff1500', '#ea3df7', '#8eff79'];
        $coloresStr = '';
        $legendsStr = '';
        $labelsStr = '';
        $dataStr = '';
        $tiempoTotal = 0;
        $tiempoInactivo = 0;
        $numIntensities = count($usuario['intensities']);
        foreach ($usuario['intensities'] as $key => $intensity) {
            $coloresStr .= str_replace('#', '', $colores[$intensity['color']]);
            $legendsStr .= str_replace(' ', '+', $intensity['text']);
            $labelsStr .= $data[$key]['tiempo'];
            $dataStr .= $data[$key]['tiempo'];
            $tiempoTotal += $data[$key]['tiempo'];
            if($key < $numIntensities - 1){
                $coloresStr .= '|';
                $legendsStr .= '|';
                $labelsStr .= '|';
                $dataStr .= ',';
            }
        }
        if($tiempoTotal < 30){
            //$tiempoInactivo = 30 - $tiempoTotal;
        }
        /* Ejemplo de formato de grafica tipo pie
        https://image-charts.com/chart?cht=p&chs=600x400&chco=1919E8|EC1010|E5E819|2FE819|19E8CC&chdl=Reduciendo+Stress|Movi%C3%A9ndose+M%C3%A1s|Perdiendo+Peso|Tonificando|Mejorando+Rendimiento&chd=a:5,1,2,5,30&chl=5|1|2|5|30&chtt=Tiempo+de+sesion+por+segmento
        */
        $imageSrc = "https://image-charts.com/chart?cht=bvs&chs=600x400&chco=" . $coloresStr . "&chl=" . $labelsStr . "&chd=a:" . $dataStr . "&chtt=Tiempo+de+sesion+por+segmentos";
        
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
            <title>Untitled Document</title></head><body>';
        $message .= '<h2>Hola ' . $usuario['nombre'] . '!</h2><h3>Este es tu informe de progreso diario del dia ' . date('d-m-Y') . '</h3><p>Tu objetivo fue el <div style="background-color:' . $colores[$usuario['_objetivo']['color']] . ';color:white">#' . $usuario['objetivo'] . ": " . $usuario['_objetivo']['objetivo'] . '</div></p><img src="' . $imageSrc . '"><div><h4>OBJETIVOS</h4><ul><li>REDUCIENDO STRES <span style="color:' . $colores[0] . '">&#9607</span></li><li>MOVIENDOSE MAS <span style="color:' . $colores[1] . '">&#9607</span></li><li>PERDIENDO PESO <span style="color:' . $colores[2] . '">&#9607</span></li><li>TONIFICANDO <span style="color:' . $colores[3] . '">&#9607</span></li><li>MEJORANDO RENDIMIENTO <span style="color:' . $colores[4] . '">&#9607</span></li></ul></div></body></html>';
        
        // $mail = mail('pagman6@gmail.com', 'Gym Test', $message);
        // To send HTML mail, the Content-type header must be set
        $cabeceras = "MIME-Version: 1.0\n";
        $cabeceras .= "Content-type: text/html; charset=ISO-8859-1\n";
        $cabeceras .= "X-Priority: 3\n";
        $cabeceras .= "X-MSMail-Priority: Normal\n";
        $cabeceras .= "X-Mailer: php\n";
        $cabeceras .= "From: \"Experiencia Play\" <info@experienciaplay.com> \n";
        $cabeceras .= "Reply-To: info@experienciaplay.com\n";
        // $cabeceras .= "Bcc: info@goldbrain.com.ar\n";
        //$mail = mail($usuario['email'], 'Experiencia Play: Reporte de Sesion ' . date('d-m-Y'), $message, $cabeceras);
        $mail = mail('axelinfante@hotmail.com', 'Experiencia Play: Reporte de Sesión', $message, $cabeceras);
        if(!$mail){
            $resp['resp'] = 'ERROR';
        }
        
        return response()->json($resp)->header('Access-Control-Allow-Origin', '*');
    }

    public function sendReporte2(Request $request){
        $resp = array( "resp" => "OK", "message" => "", "html_message" => "", "headers" => "");
        $usuario = $request->input('user');
        $data = $request->input('data');
        //$colores = $request->input('colors');
        $colores = ['#e8d500', '#009cff', '#ff1500', '#ea3df7', '#8eff79'];

        $coloresStr = '';
        $legendsStr = '';
        $labelsStr = '';
        $dataStr = '';
        $tiempoTotal = 0;
        $tiempoInactivo = 0;
        $numIntensities = count($usuario['intensities']);
        foreach ($usuario['intensities'] as $key => $intensity) {
            $tiempoTotal += $data[$key]['tiempo'];
        }
        foreach ($usuario['intensities'] as $key => $intensity) {
            $coloresStr .= str_replace('#', '', $colores[$intensity['color']]);
            $legendsStr .= '<li>' . $intensity['text'] . '<span style="color:' . $colores[$intensity['color']] . '">&#9607;</span></li>';
            // $labelsStr .= $data[$key]['tiempo']; TODO: cambiar por porcentaje
            $labelsStr .= (($data[$key]['tiempo'] * 100) / $tiempoTotal) . '%';
            $dataStr .= $data[$key]['tiempo'];
            if($key < $numIntensities - 1){
                $coloresStr .= '|';
                $labelsStr .= '|';
                $dataStr .= ',';
            }
        }
        if($tiempoTotal < 30){
            //$tiempoInactivo = 30 - $tiempoTotal;
        }
        //calculamos porcentajes de tiempo
        /* Ejemplo de formato de grafica tipo pie
        https://image-charts.com/chart?cht=p&chs=600x400&chco=1919E8|EC1010|E5E819|2FE819|19E8CC&chdl=Reduciendo+Stress|Movi%C3%A9ndose+M%C3%A1s|Perdiendo+Peso|Tonificando|Mejorando+Rendimiento&chd=a:5,1,2,5,30&chl=5|1|2|5|30&chtt=Tiempo+de+sesion+por+segmento
        */
        $imageSrc = "https://image-charts.com/chart?cht=bvs&chs=600x400&chco=" . $coloresStr . "&chl=" . $labelsStr . "&chd=a:" . $dataStr . "&chtt=Porcentaje+de+tiempo+por+segmentos";
        
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
            <title>Untitled Document</title></head><body>';
        $message .= '<h2>Hola ' . $usuario['nombre'] . '!</h2><h3>Este es tu informe de progreso diario del dia ' . date('d-m-Y') . '</h3><p>Tu objetivo fue el <div style="background-color:' . $colores[$usuario['_objetivo']['color']] . ';color:white; width: 200px">#' . $usuario['objetivo'] . ": " . $usuario['_objetivo']['objetivo'] . '</div></p><p>El tiempo total de tu entrenamiento fue de ' . $tiempoTotal . ' minutos</p><img src="' . $imageSrc . '"><div><h4>OBJETIVOS</h4><ul>' . $legendsStr . '</ul></div></body></html>';
        
        // $mail = mail('pagman6@gmail.com', 'Gym Test', $message);
        // To send HTML mail, the Content-type header must be set
        $cabeceras = "MIME-Version: 1.0\r\n";
        $cabeceras .= "Content-type: text/html; charset=ISO-8859-1\r\n";
        $cabeceras .= "X-Priority: 3\r\n";
        $cabeceras .= "X-MSMail-Priority: Normal\r\n";
        $cabeceras .= "X-Mailer: php\r\n";
        $cabeceras .= "From: \"Experiencia Play\" <informe@experienciaplay.com> \r\n";
        $cabeceras .= "To: \"User\" <pagman6@gmail.com> \r\n";
        $cabeceras .= "Reply-To: informe@experienciaplay.com\r\n";
        // $cabeceras .= "Bcc: info@goldbrain.com.ar\n";
        //$mail = mail($usuario['email'], 'Experiencia Play: Reporte de Sesion ' . date('d-m-Y'), $message, $cabeceras);
        $mail = mail('pagman6@gmail.com, axelinfante@hotmail.com', 'Experiencia Play: Reporte de Sesión', $message, $cabeceras);
        if(!$mail){
            $resp['html_message'] = $message;
            $resp['headers'] = $cabeceras;
            $resp['resp'] = 'ERROR: ' .  error_get_last()['message'];
        }
        return response()->json($resp)->header('Access-Control-Allow-Origin', '*');
    }
}
