<?php

namespace PoultryWeb;

use Illuminate\Database\Eloquent\Model;
class Banda extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mac', 'nombre'
    ];
}
